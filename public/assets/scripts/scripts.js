var owl = $('.owl-carousel');
owl.owlCarousel({
    loop: true,
    responsiveClass: true,
    margin: -40,
    center: true,
    autoplay: false,
    autoplayTimeout: 3000,
    nav: true,
    responsive: {
        0: {
            items: 1,
        },
        1200: {
            items: 3,
        },
    }
});
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});


//modals
$('.img-container.play').click(function () {
    $('#myModal').modal();
    $('#myModal').css('display', 'block');
});

$('.img-play').click(function () {
    $('#myModal2').modal();
    $('#myModal2').css('display', 'block');
});


$('.konferencja_modal').click(function (e) {
    $('#konferencja_modal_on').modal();
    $('#konferencja_modal_on').css('display', 'block');

});
$('#konferencja_modal_on .close').click(function () {
    $('.modal-backdrop').remove();
    $('#konferencja_modal_on').hide();
});

$('.about-us').click(function (e) {
    $('#aboutus').modal('toggle');
    $('#aboutus').css('display', 'block');
});
$('#aboutus .close').click(function () {
    $('.modal-backdrop').remove();
    $('#aboutus').hide();
});

$('#myModal2 .close').click(function () {
    $('.modal-backdrop').remove();
    $('#myModal2').hide();

});
$('#myModal .close').click(function () {
    $('.modal-backdrop').remove();
    $('#myModal').hide();

});




$('#menu-mobile').click(function () {
    if ($(this).hasClass('change')) {
        $('nav').removeClass('open');
        $(this).removeClass('change');

    } else {

        $('nav').addClass('open');
        $(this).addClass('change');
    }
});

new fullpage('#fullpage', {
    navigation: false,
    licenseKey: 'w2112sdadq2',
    responsiveWidth: 700,
    parallax: false,
    slideSelector: '.sliderFull',
    sectionSelector: '.section',
    afterLoad: function (origin, destination, direction) {
        if (destination.item.id == "section-briery-out") {
            $('.hideme').each(function (i) {

                $(this).animate({'opacity': '1'}, 1500);

            });
        }
    }
});

$('.arrow-down').click(function () {
    fullpage_api.moveSectionDown();
});
